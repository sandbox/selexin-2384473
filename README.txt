This module relies on the WordPress 3.8.0 file 'class-phpass.php'.

Install Instructions: Download class-phpass.php from 
https://raw.githubusercontent.com/WordPress/WordPress/3.8-branch/wp-includes/class-phpass.php
to the folder 'includes' inside this module directory 
(e.g. wordpress_passwords/includes/class-phpass.php).
